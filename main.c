#include "functions.h"

int main()
{
    // usuwanie pliku z poprzednich zamówień
    system("del ..\\project_08_35678\\orders.txt");
    system("cls");


    int mainChoice=-1, menuChoice=-1, foodChoice=-1, drinkChoice=-1, setsChoice=-1, orderChoice=-1, discount50=0;
    char blik[7] = {0};
    int num = 0, counter = 0;
    char mainChoiceChar[3], menuChoiceChar[3], foodChoiceChar[3], drinkChoiceChar[3], setsChoiceChar[3], orderChoiceChar[3], g_sizeChar[3];

    // Główne menu

    while(mainChoice != 0)
    {
        puts("Wybierz co chcesz zrobic");
        puts("1. Wyswietl menu");
        puts("2. Wyswietl moje zamowienie");
        puts("0. Wyjscie");
        printf("Wybieram: ");
        gets(mainChoiceChar);
        puts("");
        mainChoice = checkDigit(mainChoiceChar);
        switch(mainChoice)
        {
        case 1:
            // Menu jedzenia
            while(menuChoice != 0)
            {
                puts("Wybierz co chcesz zrobic");
                puts("1. Wyswietl pizze");
                puts("2. Wyswietl napoje");
                puts("3. Wyswietl zestawy");
                puts("4. Wyswietl aktualne promocje");
                puts("0. Wroc do glownego menu");
                printf("Wybieram: ");
                gets(menuChoiceChar);
                puts("");
                menuChoice = checkDigit(menuChoiceChar);
                switch(menuChoice)
                {
                case 1:
                    // Pizze
                    printf("Wybierz rozmiar:\n1. 32cm\n2. 40cm\n");
                    printf("Wybieram: ");
                    gets(g_sizeChar);
                    g_size = checkDigit(g_sizeChar);
                    if (g_size == 1) {
                        displayPizza32();
                    }
                    else if (g_size == 2) {
                        displayPizza40();
                    }
                    else {
                        printf("Wprowadzono bledna liczbe\n");
                        foodChoice = -1;
                        break;
                    }
                    puts("Jesli chcesz cos zamowic wpisz numery(enter po kazdym numerze), albo wpisz 0 aby wyjsc");
                    puts("Limitowana opcja specjalna! Pizza 50/50 - wpisz 50");
                    while(foodChoice != 0)
                    {
                        printf("Wybieram: ");
                        gets(foodChoiceChar);
                        foodChoice = checkDigit(foodChoiceChar);
                        if(foodChoice > 0 && foodChoice <= 11)
                        {
                            puts("Dodano do zamowienia");
                            g_howManyPizza++;
                            addToOrder(foodChoice, 1);
                        }
                        else if(foodChoice == 0)
                        {
                            menuChoice=-1;
                            foodChoice=0;
                        }
                        else if(foodChoice == 50)
                        {
                            puts("Wybierz numer pierwszej pizzy:");
                            while(1 == 1)
                            {
                                gets(foodChoiceChar);
                                foodChoice = checkDigit(foodChoiceChar);
                                if(foodChoice > 0 && foodChoice <= 11)
                                {
                                    puts("Dodano do zamowienia");
                                    g_fifty++;
                                    discount50++;
                                    addToOrder(foodChoice, 1);
                                    break;
                                }
                                else
                                    puts("Podaj prawidlowy numer: ");
                            }
                            puts("Wybierz numer drugiej pizzy:");
                            while(1 == 1)
                            {
                                gets(foodChoiceChar);
                                foodChoice = checkDigit(foodChoiceChar);
                                if(foodChoice > 0 && foodChoice <= 11)
                                {
                                    puts("Dodano do zamowienia");
                                    g_howManyPizza++;
                                    g_fifty++;
                                    discount50++;
                                    addToOrder(foodChoice, 1);
                                    break;
                                }
                                else
                                    puts("Podaj prawidlowy numer: ");
                            }
                        }
                        else
                        {
                            printf("\nWybrano niepoprawna opcje. Podaj poprawna liczbe!\n");
                        }
                    }
                    foodChoice=-1;
                    break;
                case 2:
                    // Napoje
                    displayDrinks();
                    puts("Jesli chcesz cos zamowic wpisz numery(enter po kazdym numerze), albo wpisz 0 aby wyjsc");
                    while(drinkChoice != 0)
                    {
                        printf("Wybieram: ");
                        gets(drinkChoiceChar);
                        drinkChoice = checkDigit(drinkChoiceChar);
                        if(drinkChoice > 0 && drinkChoice <= 5)
                        {
                            puts("Dodano do zamowienia");
                            addToOrder(drinkChoice, 2);
                        }
                        else if(drinkChoice == 0)
                        {
                            menuChoice=-1;
                            //drinkChoice=0;
                        }
                        else
                        {
                            printf("\nWybrano niepoprawna opcje. Podaj poprawna liczbe!\n");
                        }
                    }
                    drinkChoice=-1;
                    break;
                case 3:
                    // Zestawy
                    displaySets();
                    puts("Jesli chcesz cos zamowic wpisz numery(enter po kazdym numerze), albo wpisz 0 aby wyjsc");
                    while(setsChoice != 0)
                    {
                        printf("Wybieram: ");
                        gets(setsChoiceChar);
                        setsChoice = checkDigit(setsChoiceChar);
                        if(setsChoice > 0 && setsChoice <= 3)
                        {
                            puts("Dodano do zamowienia");
                            g_howManyPizza++;
                            addToOrder(setsChoice, 3);
                        }
                        else if(setsChoice == 0)
                        {
                            menuChoice=-1;
                            setsChoice=0;
                        }
                        else
                        {
                            printf("\nWybrano niepoprawna opcje. Podaj poprawna liczbe!\n");
                        }
                    }
                    setsChoice=-1;
                    break;
                case 4:
                    // Promki
                    displaySale(discount50);
                    break;
                case 0:
                    // Powrót
                    menuChoice=0;
                    break;
                default:
                    printf("Wybrano niepoprawna opcje. Podaj poprawna liczbe!\n");
                    break;
                }
            }
            menuChoice=-1;
            break;
        case 2:
            // Wyswietla zamowienie
            displayOrders();
            if(g_isEmpty == 0)
            {
                puts("Wpisz 1 aby potwierdzic zamowienie i zaplacic albo 0 aby wyjsc");
                printf("Wybieram: ");
                gets(orderChoiceChar);
                orderChoice = checkDigit(orderChoiceChar);
                if (orderChoice == 1)
                {
                    counter = 0;
                    while (counter < 6) {
                        printf("Wpisz poprawny kod blik aby zaplacic za zamowienie: ");
                        gets(blik);
                        for (int i = 0; blik[i] != '\0'; i++) {
                            if (strlen(blik) == 6 && 0 != isdigit(blik[i])) {
                                num = atoi(blik);
                            }
                            else
                            {
                                counter++;
                                break;
                            }

                        }
                        if (num > 99999 && num < 1000000) {
                            puts("Platnosc przyjeta - generowanie paragonu");
                            receipt();
                            puts("Dziekujemy za skorzystanie z naszych uslug");
                            mainChoice = 0;
                            break;
                        }
                        else
                        {
                            if (counter == 5)
                            {
                                printf("Wprowadzono zbyt duzo niepoprawnych kombinacji\n\n");
                                menuChoice = -1;
                                setsChoice = 0;
                                mainChoice = -1;

                                break;
                            }
                            else if (counter > 0) // licznik max 5 szans
                            {
                                printf("To jest twoja %i nieudana proba!\n", counter);
                            }
                        }
                    }
                }
            }
            else
            {
                puts("Wyjscie");
                mainChoice=-1;
                orderChoice=-1;
            }
            break;
        case 0:
            // Wyjście z programu
            puts("Wyjscie");
            puts("Zapraszamy ponownie");
            mainChoice=0;
            break;
        default:
            printf("Wybrano niepoprawna opcje. Podaj poprawna liczbe!\n");
            break;
        }
    }
    return 0;
}

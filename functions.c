#include "functions.h"

float g_pay=0;          // kwota do zapłaty - zmienna globalna
int g_howManyPizza=0;   // if >= 2 darmowe pepsi
int g_isEmpty=0;        // if 0 zamówienie jest puste
int g_fifty=0;          // pizza 50/50
float g_fiftyPay=0;     // koszt dwóch pizzy /2
int g_size = 0;         // rozmiar pizzy

// Zabezpieczenie
int checkDigit(char x[])
{
    int y = 0;
    if(strlen(x) > 0 && strlen(x) < 3)
    {
        for(int i=0;x[i] != '\0';i++)
        {
            int asd = isdigit(x[i]);
            if(isdigit(x[i]) != 0)
                y = atoi(x);
            else
                return -1;
        }
        return y;
    }
    else
    {
        return -1;
    }
    return -1;
}

// Wyświetla pizze o rozmiarze 32
void displayPizza32(){
    FILE *fpizza32;

    fpizza32 = fopen("..\\project_08_35678\\pizze32.txt","r");
    if(fpizza32 == NULL){
        puts("Error");
        fclose(fpizza32);
    }

    char pizze32[SIZE];
    while(fgets(pizze32, SIZE, fpizza32))
        puts(pizze32);

    fclose(fpizza32);
}

// Wyświetla pizze o rozmiarze 40
void displayPizza40() {
    FILE* fpizza40;

    fpizza40 = fopen("..\\project_08_35678\\pizze40.txt", "r");
    if (fpizza40 == NULL) {
        puts("Error");
        fclose(fpizza40);
    }

    char pizze40[SIZE];
    while (fgets(pizze40, SIZE, fpizza40))
        puts(pizze40);

    fclose(fpizza40);
}

// Wyświetla napoje
void displayDrinks(){
    FILE *fdrinks;
    char drinks[SIZE];
    fdrinks = fopen("..\\project_08_35678\\drinks.txt","r");
    if(fdrinks == NULL){
        puts("Error");
        fclose(fdrinks);
    }
    else
    {
        while(fgets(drinks, SIZE, fdrinks))
            puts(drinks);
        fclose(fdrinks);
    }

}

// Wyświetla zestawy
void displaySets(){
    FILE *fsets;
    char sets[SIZE];
    fsets = fopen("..\\project_08_35678\\sets.txt","r");
    if(fsets == NULL){
        puts("Error");
        fclose(fsets);
    }
    else
    {
        while(fgets(sets, SIZE, fsets))
            puts(sets);
        fclose(fsets);
    }

}

// Wyświetla promocje
void displaySale(int discount50){
    FILE *fsale;
    char sale[SIZE];
    fsale = fopen("..\\project_08_35678\\sales.txt","r");
    if(fsale == NULL){
        puts("Error");
        fclose(fsale);
    }
    else
    {
        fgets(sale, SIZE, fsale);
        puts(sale);

        if(discount50 > 0)
        {
            fgets(sale, SIZE, fsale);
            sale[3] = 'X';
        }
        else{
            fgets(sale, SIZE, fsale);
        }
        puts(sale);
        if(g_howManyPizza >= 2)
        {
            fgets(sale, SIZE, fsale);
            sale[3] = 'X';
        }
        else{
            fgets(sale, SIZE, fsale);
        }
        puts(sale);
        if(g_pay > 100)
        {
            fgets(sale, SIZE, fsale);
            sale[3] = 'X';
        }
        else{
            fgets(sale, SIZE, fsale);
        }
        puts(sale);
        fgets(sale, SIZE, fsale);
        puts(sale);

        fclose(fsale);
    }
}

// Wyświetla zamówienie
void displayOrders(){
    FILE *forders;

    forders = fopen("..\\project_08_35678\\orders.txt","r");
    if(forders == NULL){
        printf("Twoje zamowienie jest puste \n \n");
        g_isEmpty=1;
        //fclose(forders);
    }
    if(g_isEmpty==0)
    {
        char orders[SIZE];
        while(fgets(orders, SIZE, forders))
            puts(orders);
        if(g_pay > 100 && g_howManyPizza >= 2 && g_fiftyPay > 0)
        {
            printf("Napoj gratis: %40.2fzl \n", -5.00);
            printf("Znizka powyzej 100zl: %33d%% \n", -20);
            printf("Pizza 50/50: %41.2fzl \n", -g_fiftyPay);
            printf("Do zaplaty: %42.2fzl\n", g_pay*0.8);
        }
        else if(g_pay > 100 && g_fiftyPay > 0)
        {
            printf("Znizka powyzej 100zl: %33d%% \n", -20);
            printf("Pizza 50/50: %41.2fzl \n", -g_fiftyPay);
            printf("Do zaplaty: %42.2fzl\n", g_pay*0.8);
        }
        else if(g_howManyPizza >= 2 && g_fiftyPay > 0)
        {
            printf("Napoj gratis: %40.2fzl \n", -5.00);
            printf("Pizza 50/50: %41.2fzl \n", -g_fiftyPay);
            printf("Do zaplaty: %42.2fzl\n", g_pay);
        }
        else if(g_howManyPizza >= 2 && g_pay > 100)
        {
            printf("Napoj gratis: %40.2fzl \n", -5.00);
            printf("Znizka powyzej 100zl: %33d%% \n", -20);
            printf("Do zaplaty: %42.2fzl\n", g_pay*0.8);
        }
        else if(g_pay > 100 )
        {
            printf("Znizka powyzej 100zl: %33d%% \n", -20);
            printf("Do zaplaty: %42.2fzl\n", g_pay*0.8);
        }
        else if(g_howManyPizza >= 2)
        {
            printf("Napoj gratis: %40.2fzl \n", -5.00);
            printf("Do zaplaty: %42.2fzl\n", g_pay);
        }
        else if(g_fiftyPay > 0)
        {
            printf("Pizza 50/50: %41.2fzl \n", -g_fiftyPay);
            printf("Do zaplaty: %42.2fzl\n", g_pay);
        }
        else
        {
            printf("Do zaplaty: %42.2fzl\n", g_pay);
        }
        g_isEmpty = 0;
        fclose(forders);
    }
    else{
        printf("Do zaplaty: %42.2fzl\n", g_pay);
    }

}


// Dodawanie produktów do zamówienia
void addToOrder(int myChoice, int whichChoice){
    g_isEmpty=0;
    FILE *ftoOrder;
    ftoOrder = fopen("..\\project_08_35678\\orders.txt","a");

    char toOrder[SIZE];

    if(whichChoice == 1)
    {
        FILE* fpizza32;
        fpizza32 = fopen("..\\project_08_35678\\pizze32.txt", "r");
        FILE* fpizza40;
        fpizza40 = fopen("..\\project_08_35678\\pizze40.txt", "r");

        for(int i=0;i<=myChoice;i++)
        {
            if (g_size == 1) {

                fgets(toOrder, SIZE, fpizza32);

            }
            else if (g_size == 2) {

                fgets(toOrder, SIZE, fpizza40);

            }
        }
        swapSigns(toOrder);
        fprintf(ftoOrder, "%s", toOrder);
        addPrice(whichChoice, myChoice);

        fclose(fpizza32);
        fclose(fpizza40);
    }
    else if(whichChoice == 2)
    {

        FILE* fdrinks;
        fdrinks = fopen("..\\project_08_35678\\drinks.txt", "r");

        for(int i=0;i<myChoice;i++)
        {
            fgets(toOrder, SIZE, fdrinks);
        }
        swapSigns(toOrder);
        fprintf(ftoOrder, "%s", toOrder);
        addPrice(whichChoice, myChoice);

        fclose(fdrinks);
    }
    else if(whichChoice == 3)
    {

        FILE* fsets;
        fsets = fopen("..\\project_08_35678\\sets.txt", "r");

        for(int i=0;i<myChoice;i++)
        {
            fgets(toOrder, SIZE, fsets);
        }
        swapSigns(toOrder);
        fprintf(ftoOrder, "%s", toOrder);
        addPrice(whichChoice, myChoice);

        fclose(fsets);
    }
    if (g_howManyPizza == 2)
    {
        g_howManyPizza++;
        FILE* fdrinks;
        fdrinks = fopen("..\\project_08_35678\\drinks.txt", "r");

        fgets(toOrder, SIZE, fdrinks);

        swapSigns(toOrder);
        fprintf(ftoOrder, "%s", toOrder);

        fclose(fdrinks);
    }   

    fclose(ftoOrder);
    
}

// Dodawanie i sumowanie cen za wybrane produkty
void addPrice(int x, int y)
{
    FILE *fprices;
    fprices = fopen("..\\project_08_35678\\prices.txt","r");
    float price = 0;
    for(int i=1;i<=10*(x-1)+y;i++)
    {
        fscanf(fprices, "%f", &price);
    }
    if(g_fifty == 0 || g_fifty > 2)
        g_pay += price;
    else{
        g_fiftyPay += price/2;
        g_pay += price/2;
        g_fifty = 0;
    }
    if (g_size == 2) {
        g_pay += 4;

    }

}

// Zmiana znaków żeby ładniej wyświetlić produkty
void swapSigns(char toOrder[])
{
    toOrder[0]=42;
    toOrder[1]=32;
    toOrder[2]=32;
}

// Drukowanie paragonu
void receipt()
{
    FILE *freceipt;
    freceipt = fopen("..\\project_08_35678\\receipt.txt","w");
    FILE *ffromOrder;
    ffromOrder = fopen("..\\project_08_35678\\orders.txt","r");

    char fromOrders[SIZE];

    fprintf(freceipt, "*************Pizzeria - 'Nie tanie i nie za dobre'************* \n" );
    fprintf(freceipt, "Tarnow Spolka Komandytowa\n" );
    fprintf(freceipt, "Mickiewicza 8, 33-100 Tarnow\n" );
    fprintf(freceipt, "NIP 701-02-30-142\n" );
    fprintf(freceipt, "20.04.2022 4:20\n" );
    fprintf(freceipt, "PARAGON NIE-FISKALNY\n" );
    fprintf(freceipt, "----------------------\n" );
    while(fgets(fromOrders, SIZE, ffromOrder))
        fprintf(freceipt, "%s",fromOrders);

    fprintf(freceipt, "\n");

    if(g_pay > 100 && g_howManyPizza >= 2 && g_fiftyPay > 0)
    {
        fprintf(freceipt,"Napoj gratis: %40.2fzl \n", -5.00);
        fprintf(freceipt,"Znizka powyzej 100zl: %33d%% \n", -20);
        fprintf(freceipt,"Pizza 50/50: %41.2fzl \n", -g_fiftyPay);
        fprintf(freceipt,"Do zaplaty: %42.2fzl\n", g_pay*0.8);
    }
    else if(g_pay > 100 && g_fiftyPay > 0)
    {
        fprintf(freceipt,"Znizka powyzej 100zl: %33d%% \n", -20);
        fprintf(freceipt,"Pizza 50/50: %41.2fzl \n", -g_fiftyPay);
        fprintf(freceipt,"Do zaplaty: %42.2fzl\n", g_pay*0.8);
    }
    else if(g_howManyPizza >= 2 && g_fiftyPay > 0)
    {
        fprintf(freceipt,"Napoj gratis: %40.2fzl \n", -5.00);
        fprintf(freceipt,"Pizza 50/50: %41.2fzl \n", -g_fiftyPay);
        fprintf(freceipt,"Do zaplaty: %42.2fzl\n", g_pay);
    }
    else if(g_howManyPizza >= 2 && g_pay > 100)
    {
        fprintf(freceipt,"Napoj gratis: %40.2fzl \n", -5.00);
        fprintf(freceipt,"Znizka powyzej 100zl: %33d%% \n", -20);
        fprintf(freceipt,"Do zaplaty: %42.2fzl\n", g_pay*0.8);
    }
    else if(g_pay > 100 )
    {
        fprintf(freceipt,"Znizka powyzej 100zl: %33d%% \n", -20);
        fprintf(freceipt,"Do zaplaty: %42.2fzl\n", g_pay*0.8);
    }
    else if(g_howManyPizza >= 2)
    {
        fprintf(freceipt,"Napoj gratis: %40.2fzl \n", -5.00);
        fprintf(freceipt,"Do zaplaty: %42.2fzl\n", g_pay);
    }
    else if(g_fiftyPay > 0)
    {
        fprintf(freceipt,"Pizza 50/50: %41.2fzl \n", -g_fiftyPay);
        fprintf(freceipt,"Do zaplaty: %42.2fzl\n", g_pay);
    }
    else
    {
        fprintf(freceipt,"Do zaplaty: %42.2fzl\n", g_pay);
    }
    fprintf(freceipt, "----------------------\n" );
    fprintf(freceipt, "00032   # 26214 175     10.03.2022 22:56\n" );
    fprintf(freceipt, "Platnosc: BLIK\n" );
    fprintf(freceipt, "C3254234FSD65FR96RWQR798RWQ679WQR6789Q6WWQ679WQR6789Q6WWQ679WQR\n" );
    fclose(freceipt);

    system("..\\project_08_35678\\receipt.txt");    // Automatyczne otwieranie pliku w którym jest paragon
}


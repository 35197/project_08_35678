#ifndef FUNCTIONS_H
#define FUNCTIONS_H
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#define SIZE 70                                     //rozmiar tablicy znaków


extern float g_pay;                                 // kwota do zapłaty - deklaracja zmiennej globalnej
extern int g_howManyPizza;                          // if >= 2 darmowe pepsi
extern int g_isEmpty;                               // czy zamówienie jest puste
extern int g_fifty;                                 // pizza 50/50
extern float g_fiftyPay;                            // koszt dwóch pizzy /2
extern int g_size;									// rozmiar pozzy

void displayPizza32();                              // Wyświetla pizze o rozmiarze 32
void displayPizza40();                              // Wyświetla pizze o rozmiarze 40
void displayDrinks();                               // Wyświetla napoje
void displaySets();                                 // Wyświetla zestawy
void displaySale(int);                              // Wyświetla promocje
void displayOrders();                               // Wyświetla zamówienie
void addToOrder(int foodChoice, int whichChoice);   // Dodaje do zamówienia
void addPrice(int, int);                            // Dodaje i sumuje ceny produktów
void swapSigns(char toOrder[]);                     // Zamienia znaki żeby ładniej wyglądało przy wypisaniu produktu
void receipt();                                     // Wyświetla paragon z zamówieniem
int checkDigit(char []);                            // Zabezpieczenie

#endif // FUNCTIONS_H

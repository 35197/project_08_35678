# Dokumentacja projektu Pizzeria

## Autor
Paweł Kamiński

Jakub Dojka

Jakub Kmiecik

## Spis treści
1. Instrukcja
2. Narzędzia do tworzenia
3. Podział prac

## 1. Instrukcja
Podstawowe sterowanie polega na wpisaniu wybranego numeru i potwierdzeniu wyboru poprzez naciśnięcie enter numerem, odpowiedzialnym za wstecz jest zero (0).
Pierwszym krokiem po uruchomieniu programu jest wybór tego co chcemy zrobić. Nie zamówiliśmy jeszcze nic. Po wybraniu opcji numer jeden przejdziemy do składania zamówienia. Opcja druga (2) wyświetla zamówienie. W tej chwili nie mamy jeszcze wybranych produktów. Opcja trzecia (3) zamyka program. Aby złożyć zamówienie wybieramy opcję numer jeden (1). Natomiast aby cofnąć się wybieramy zero (0).

![Screenshot](img/1.png)

Następnym krokiem jest podjęcie decyzji co chcemy zamówić. Mamy różne opcje: jedynkę (1) czyli pizzę, dwa (2) czyli napoje, trzy (3) czyli zestawy i cztery (4), w której sprawdzimy aktualnie dostępne promocje.

![Screenshot](img/2.png)

Kolejny krok to wybór rozmiaru pizzy. Obecnie dostępne są dwa rozmiary. Składnia kodu pozostaje bez zmian, zmienia się odczytywanie pliku z pizzy o rozmiarach 32 na 40, ceny pizzy w większym rozmiarze są wyższe niż w niższym (wybieram pizzę o rozmiarze 32, wybieramy opcję numer jeden (1)). 

![Screenshot](img/3.png)

Teraz możemy wybrać naszą pizzę wpisując konkretny numer od 1 do 10. 

![Screenshot](img/4.png)

Nasza pizza została dodana, ale możemy od razu skorzystać z promocji. Aby to zrobić wpisujemy pięćdziesiąt (50).

![Screenshot](img/5.png)

Teraz wybieramy dwie pizze z listy wyżej i gotowe.

![Screenshot](img/6.png)

Wracamy do menu głównego aby wybrać napoje. Wpisujemy zero (0) a gdy będziemy w menu wyboru wpisujemy dwa (2).

![Screenshot](img/7.png)

Teraz wybieramy dostępny zestaw dzięki opcji numer trzy (3). 

![Screenshot](img/8.png)

Aby przejrzeć aktywne promocje przechodzimy do punktu cztery (4). Jeżeli nasz wybór spełnia założenia promocji, ta się automatycznie aktywuje i jest uwzględniana przy płatności.

![Screenshot](img/9.png)

Jeżeli skończyliśmy zamówienie, wracamy do menu głównego klikając zero (0) aby sprawdzić nasze wybory i dokonać płatności, wówczas wybieramy opcję drugą (2).
Sprawdzamy zamówienie i podajemy kod blik aby zapłacić. Kod blik wymaga wprowadzenia 6 cyfr.

![Screenshot](img/10.png)

Nasza płatność została zaakceptowana a program drukuje automatycznie paragon
![Screenshot](img/11.png)


## 2. Narzędzia do tworzenia
[O języku C](https://pl.wikipedia.org/wiki/C_(język_programowania))

[Bitbucket](https://pl.wikipedia.org/wiki/Bitbucket)

[Qt Creator](https://pl.wikipedia.org/wiki/Qt_Creator)

[Visual Studio](https://pl.wikipedia.org/wiki/Microsoft_Visual_Studio)

[Git](https://pl.wikipedia.org/wiki/Git_(oprogramowanie))

## 3. Podział prac
![Screenshot](img/12.png)